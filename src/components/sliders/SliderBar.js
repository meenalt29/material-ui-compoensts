import React from "react";
import Slider from "@material-ui/core/Slider";

const SliderBar = () => {
  const [value, setValue] = React.useState(30);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div style={{ width: "400px", marginLeft: "60px" }}>
      <h1>Slider component</h1>
      <Slider
        value={value}
        onChange={handleChange}
        aria-labelledby="continuous-slider"
        color="secondary"
      />
    </div>
  );
};

export default SliderBar;
