import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginLeft:'auto',
      marginRight:'auto',
      marginTop: 'auto'
    },
  },
}));
// ['Home','chips', 'buttons','card']
export default function Buttons() {
  const classes = useStyles();

  return (
    <div className={classes.root} >
      <Button variant="contained">Default</Button>
      <Button variant="contained" color="primary">
        Primary
      </Button>
      <Button variant="contained" color="secondary">
        Secondary
      </Button>
      <Button variant="contained" disabled>
        Disabled
      </Button>
      <Button variant="contained" color="primary" href="#contained-buttons" style={{borderRadius:'24PX'}}>
        Link
      </Button>
    </div>
  );
}
