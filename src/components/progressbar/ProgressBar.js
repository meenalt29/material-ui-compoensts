import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import LinearProgress from "@material-ui/core/LinearProgress";

const ProgressBar = () => {
  return (
    <>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div>
          <CircularProgress />
          <CircularProgress color="secondary" />
        </div>
        <div style={{ width: "400px", marginLeft: "60px" }}>
          <LinearProgress />
          <LinearProgress color="secondary" style={{ marginTop: "50px" }} />
        </div>
      </div>
    </>
  );
};

export default ProgressBar;
