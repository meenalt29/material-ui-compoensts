import { Tooltip, Button, Zoom } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

import React from "react";
const Tooltips = () => {
  return (
    <div style={{ marginLeft: "300px", marginTop: "100px" }}>
      <Tooltip title="tooltip example">
        <Button variant="outlined">Tooltip</Button>
      </Tooltip>
      <Tooltip title="Tooltip with arrow" arrow style={{ marginLeft: "10px" }}>
        <Button variant="outlined">
          <DeleteIcon color="primary" />
        </Button>
      </Tooltip>

      <Tooltip
        title="Tooltip top"
        arrow
        style={{ marginLeft: "10px" }}
        placement="top"
      >
        <Button variant="outlined">top</Button>
      </Tooltip>

      <Tooltip
        title="Tooltip Bottom"
        arrow
        style={{ marginLeft: "10px" }}
        placement="bottom"
      >
        <Button variant="outlined">Bottom</Button>
      </Tooltip>

      <Tooltip
        title="Tooltip Left"
        arrow
        style={{ marginLeft: "10px" }}
        placement="left"
      >
        <Button variant="contained" color="secondary">
          Left
        </Button>
      </Tooltip>

      <Tooltip
        title="Tooltip Right"
        arrow
        style={{ marginLeft: "10px" }}
        placement="right"
        TransitionComponent={Zoom}
      >
        <Button variant="contained">Right</Button>
      </Tooltip>
    </div>
  );
};

export default Tooltips;
