
import './App.css';
import AppBottom from './components/appbottom/appbottom';
import AppTop from './components/apptop/apptop';
import Backdrop from './components/backdrop/backdrop';
import Grid from '@material-ui/core/Grid';
import Cards from './components/card/cards';
import Buttons from './components/buttons/button';
import TitlebarGridList from './components/gridlist/gridlist';
import CheckboxLabels from './components/checkboxes/checkbox';
import FloatingActionButtonZoom from './components/buttons/floatingbutton';
import LabelBottomNavigation from './components/bottomnavigation/bottomnavigation';
import ChipsArray from './components/chips/arraychip';
import Chips from './components/chips/fillchips';
import OutlinedChips from './components/chips/outlinechips';
import CustomPaginationActionsTable from './components/tables/datatable';
import StickyHeadTable from './components/tables/datable2';
import ToolbarGrid from './components/toolbar/toolbar';
// import PersistentDrawerLeft from './components/drawer/drawer';
// import DateTime from './components/date picker/DateTimePicker'
import DateAndTimePickers from './components/date picker/datetimewen';
import SimpleDialogDemo from './components/dialogs/dialog';
import AlertDialog from './components/dialogs/dialogalert';
import FormDialog from './components/dialogs/dialogform';
import ResponsiveDialog from './components/dialogs/dialogresponsive';
import ConfirmationDialog from './components/dialogs/dialogcondirm';
import ListDividers from './components/divider/divider';
import InsetDividers from './components/divider/divider2';
import SubheaderDividers from './components/divider/subheadeddivider';
import MiddleDividers from './components/divider/mddel';
import SwipeableTemporaryDrawer from './components/drawer/drawer';
// import ImageGridList from './components/image list/imagelist';
import FolderList from './components/list/folderlist';
import InteractiveList from './components/list/interactivelist';
import SelectedListItem from './components/list/selectlist';
import AlignItemsList from './components/list/alignlist';
import SwitchListSecondary from './components/list/switchlist';
import  CustomizedMenus from './components/menu/custommenu'
import EnhancedTable from "./components/tables/DataTable3";
import ProgressBar from './components/progressbar/ProgressBar';
import BasicTextFields from './components/textfileds/BasicTextFileds';
import FormPropsTextFields from './components/textfileds/textfliedmsg';
import RadioButton from './components/radiobuttons/RadioButton';
import SliderBar from './components/sliders/SliderBar';
import TransitionsSnackbar from './components/snackbar/Snackbar'
import CustomizedSnackbars from './components/snackbar/SnackBar2';
import Switches from './components/swtiches/Switches'
import Tooltips from './components/tooltip/Tooltips';
import { Switch, Route } from 'react-router-dom';


import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function App(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
          {['home','app top','app bottom','bottom navigation','chips', 'array chips','outline chips','checkboxes','buttons','floating buttons','backdrop', 'card','data table','data table2 fixed header','data table3','grid list','drawer','date and time picker web','dialog simple','dialog alert','dialog form','dialog responsive','dialog confirm','list divider','inset divider','subdivider divider','middel divider','folder list','interactive list','select list','align list','switch list','custom menu','ProgressBar','BasicTextFileds','FormPropsTextFields','RadioButton','SliderBar','Switches','Tooltips','SnackBar','CustomizedSnackbars','toolbar'].map((text, index) => (
            <Link to={text} style={{textDecoration:'none',color:'black'}}>
              <ListItem button key={text}>
                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            </Link>
          ))}
        </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Responsive drawer
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route exact path='/'></Route>
          <Route exact path='/chips' component={Chips}></Route>
          <Route exact path='/buttons' component={Buttons}></Route>
          <Route exact path='/card' component={Cards}></Route>
          <Route exact path='/grid list' component={TitlebarGridList}></Route>
          <Route exact path='/array chips' component={ChipsArray}></Route>
          <Route exact path='/outline chips' component={OutlinedChips}></Route>
          <Route exact path='/app bottom' component={AppBottom}></Route>
          <Route exact path='/app top' component={AppTop}></Route>
          <Route exact path='/backdrop' component={Backdrop}></Route>
          <Route exact path='/checkboxes' component={CheckboxLabels}></Route>
          <Route exact path='/drawer' component={SwipeableTemporaryDrawer}></Route>
          <Route exact path='/data table' component={CustomPaginationActionsTable}></Route>
          <Route exact path='/data table2 fixed header' component={StickyHeadTable}></Route>
          <Route exact path="/data table3" component={EnhancedTable}></Route>
          <Route exact path='/floating buttons' component={FloatingActionButtonZoom}></Route>
          <Route exact path='/bottom navigation' component={LabelBottomNavigation}></Route>
          <Route exact path='/toolbar' component={ToolbarGrid}></Route>
          {/* <Route exact path='/date and time picker' component={MaterialUIPickers}></Route> */}
          <Route exact path='/date and time picker web' component={DateAndTimePickers}></Route>
          <Route exact path='/dialog simple' component={SimpleDialogDemo}></Route>
          <Route exact path='/dialog alert' component={AlertDialog}></Route>
          <Route exact path='/dialog form' component={FormDialog}></Route>
          <Route exact path='/dialog responsive' component={ResponsiveDialog}></Route>
          <Route exact path='/dialog confirm' component={ConfirmationDialog}></Route>
          <Route exact path='/list divider' component={ListDividers}></Route>
          <Route exact path='/inset divider' component={InsetDividers}></Route>
          <Route exact path='/subdivider divider' component={SubheaderDividers}></Route>
          <Route exact path='/middel divider' component={MiddleDividers}></Route>
          {/* <Route exact path='/image list' component={ImageGridList}></Route> */}
          <Route exact path='/folder list' component={FolderList}></Route>
          <Route exact path='/interactive list' component={InteractiveList}></Route>
          <Route exact path='/select list' component={SelectedListItem}></Route>
          <Route exact path='/align list' component={AlignItemsList}></Route>
          <Route exact path='/switch list' component={SwitchListSecondary}></Route>
          <Route exact path='/custom menu' component={CustomizedMenus}></Route>
          {/* <Route exact path='/date time picker' component={DateTime}></Route> */}
          <Route exact path='/ProgressBar' component={ProgressBar}></Route>
          <Route exact path='/BasicTextFileds' component={BasicTextFields}></Route>
          <Route exact path='/RadioButton' component={RadioButton}></Route>
          <Route exact path='/SliderBar' component={SliderBar}></Route>
          <Route exact path='/Switches' component={Switches}></Route>
          <Route exact path='/Tooltips' component={Tooltips}></Route>
          <Route exact path='/SnackBar' component={TransitionsSnackbar}></Route>
          <Route exact path='/CustomizedSnackbars' component={CustomizedSnackbars}></Route>
          <Route exact path='/FormPropsTextFields' component={FormPropsTextFields}></Route>




        </Switch>
      </main>
    </div>
  );
}

App.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default App;
